import React from 'react';
import PropTypes from 'prop-types';
import cc from 'classcat';

const DepartureRow = (props) => {
  const {
    translate,
    departure,
    thereafter,
  } = props;

  return (
    <tr className='small departure'>
      <td
        className='departure__line'
        style={{
          width: '50px',
          color: departure.colors.foreground,
          backgroundColor: departure.colors.background,
        }}
      >
        {departure.line.shortName}
      </td>
      <td className='departure__direction'>
        {departure.direction.short}
      </td>
      <td
        className='departure__track'
      >
        {departure.track}
      </td>
      <td
        className={cc([
          'departure__next',
          {
            'departure__next--now': departure.departure.wait.minutes <= 0,
            'departure__next--urgent': (
              departure.departure.wait.minutes <= 5
              && departure.departure.wait.minutes > 0
            ),
          },
        ])}
      >
        {
          departure.departure.wait.minutes <= 0
          ? translate('NOW')
          : departure.departure.wait.minutes
        }
      </td>
      {
        thereafter &&
        <td
          className={cc([
            'departure__thereafter',
            {
              'departure__thereafter--now': thereafter.departure.wait.minutes <= 0,
              'departure__thereafter--urgent': (
                thereafter.departure.wait.minutes <= 5
                && thereafter.departure.wait.minutes > 0
              ),
            },
          ])}
        >
          {
            thereafter.departure.wait.minutes <= 0
            ? translate('NOW')
            : thereafter.departure.wait.minutes
          }
        </td>
      }
      {
        !thereafter &&
        <td
          className='departure__thereafter departure__thereafter--none'
        />
      }
    </tr>
  );
};

DepartureRow.propTypes = {
  thereafter: PropTypes.object,
  departure: PropTypes.object.isRequired,
};

DepartureRow.defaultProps = {
  thereafter: null,
};

export default DepartureRow;
