import React from 'react';
import PropTypes from 'prop-types';

import firebase from 'firebase/app';

import DepartureRow from './DepartureRow';

class DepartureTable extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      stop: null,
      error: null,
      stopRef: null,
      departures: null,
      snapshotNotExistsCount: 0,
    };

    this.db = firebase.database();

    this.stopRefValueCallback = this.stopRefValueCallback.bind(this);
  }

  stopRefValueCallback(snapshot) {
    if (!snapshot.exists()) {
      this.setState((state) => ({
        ...state,
        error: 'STOPDOESNOTEXIST',
      }));

      this.state.stopRef.off('value', this.stopRefValueCallback);

      return;
    }

    this.setState((state) => ({
      ...state,
      ...snapshot.val(),
      snapshotNotExistsCount: 0,
    }));
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.stopSlug !== this.props.stopSlug) {
      this.state.stopRef.off('value', this.stopRefValueCallback);

      const stopRef = this.db.ref(`/vasttrafik/departures/${nextProps.stopSlug}`);

      this.setState((state) => ({
        ...state,
        stopRef,
      }));

      stopRef.on('value', this.stopRefValueCallback);
    }
  }

  componentDidMount() {
    const stopRef = this.db.ref(`/vasttrafik/departures/${this.props.stopSlug}`);

    this.setState((state) => ({
      ...state,
      stopRef,
    }));

    stopRef.on('value', this.stopRefValueCallback);
  }

  render() {
    const {
      translate,
    } = this.props;

    const {
      stop,
      error,
      departures,
    } = this.state;

    if (error) {
      return (
        <div>
          <p className='error small'>
            {translate(error)}
          </p>
        </div>
      );
    }

    if (!stop && !departures) {
      return (
        <div className='vasttrafik-spinner' />
      );
    }

    if (stop && !departures) {
      return (
        <p className='error small'>
          {translate('NODEPARTURESFOUND')}
        </p>
      );
    }

    return (
      <div>
        <table className='module-vasttrafik__departures-table'>
          <thead>
            <tr>
              <th>
                {translate('LINE')}
              </th>
              <th>
                {translate('DESTINATION')}
              </th>
              <th>
                {translate('TRACK')}
              </th>
              <th>
                {translate('NEXT')}
              </th>
              <th>
                {translate('THEREAFTER')}
              </th>
            </tr>
          </thead>
          <tbody>
            {
              Object.keys(departures).map((departureLine) => {
                const lineDepartures = departures[departureLine];

                return Object.keys(lineDepartures).map((destinationDeparturesKey) => {
                  const [
                    departure,
                    thereafter,
                  ] = lineDepartures[destinationDeparturesKey];

                  return (
                    <DepartureRow
                      key={destinationDeparturesKey}
                      translate={translate}
                      departure={departure}
                      thereafter={thereafter}
                    />
                  );
                });
              })
            }
          </tbody>
        </table>
      </div>
    );
  }
}

DepartureTable.propTypes = {
  stopSlug: PropTypes.string.isRequired,
  translate: PropTypes.func.isRequired,
};

export default DepartureTable;
