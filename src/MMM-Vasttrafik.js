/* globals Module, Log */

import firebase from 'firebase/app';
import 'firebase/database';

import MMMVasttrafik from './index';
import './MMM-Vasttrafik.scss';

Module.register('MMM-Vasttrafik', {
	defaults: {
		vasttrafik: {
			stop: null,
			useLongStopName: false,
		},
		firebase: {
			apiKey: null,
			authDomain: null,
			databaseURL: null,
			projectId: null,
			storageBucket: null,
			messagingSenderId: null,
		},
	},
	getStyles() {
		return [
			'MMM-Vasttrafik.css',
		];
	},
	getTranslations() {
		return {
			en: 'translations/en.json',
			sv: 'translations/sv.json',
		};
	},
  getDom() {
    if (!this.templateData.stop) {
      return null;
    }

    const wrapper = document.createElement('div');
    wrapper.classList.add('module-vasttrafik');

    MMMVasttrafik.render(wrapper, {
      stop: this.config.vasttrafik.stop,
      error: this.templateData.error,
      translate: this.templateData.translate,
    });

    return wrapper;
  },
	getHeader() {
		if (this.templateData.stop.name) {
			const stopName = this.config.vasttrafik.useLongStopName
				? this.templateData.stop.name
				: this.templateData.stop.shortName;

			return this.translate('DEPARTURESFROMSTOP', {
				stop: stopName,
			});
		} else {
			return this.translate('DEPARTURES');
		}
	},
	start() {
		Log.info(`Starting module: ${this.name}`);

		this.templateData = {
			error: false,
			loading: true,
			stop: {},
			departures: [],
			translate: (...args) => {
				return this.translate(...args);
			},
		};

		const vasttrafik = this.config.vasttrafik;

		if (!vasttrafik.stop || typeof vasttrafik.stop !== 'string' || vasttrafik.stop.length < 1) {
			this.templateData.error = this.translate('SETSTOP');
			return;
		}

		try {
      if (firebase.apps.length === 0) {
        firebase.initializeApp(this.config.firebase);
      }
		}
		catch (error) {
			this.templateData.error = this.translate('INVALIDFIREBASECONFIG');
			return;
		}
	},
});
