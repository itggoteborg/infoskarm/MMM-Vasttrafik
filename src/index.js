
import React from 'react';
import ReactDOM from 'react-dom';

import DepartureTable from './components/DepartureTable';

const MMMVasttrafik = {
  render: (element, { stop, error, translate }) => {
    return ReactDOM.render(
      <div>
        {
          error
          ? (
            <p className='small error'>
              {error}
            </p>
          )
          : (
            <DepartureTable
              stopSlug={stop}
              translate={translate}
            />
          )
        }
      </div>,
      element
    );
  },
};

export default MMMVasttrafik;
